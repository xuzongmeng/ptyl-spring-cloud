package com.forezp.servicehi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class UserApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }

    @Value("${server.port}")
    String port;

    @GetMapping("/info")
    public String user(@RequestParam(value = "name", defaultValue = "forezp") String name) {
        return "hi user " + name + " ,i am from port:" + port;
    }

    @Value("${name}")
    String name;

    @GetMapping("/name")
    public String name() {
        return "current user is" + name;
    }
//    @RequestMapping("/user/info")
//    public String user(@RequestParam(value = "name", defaultValue = "forezp") String name) {
//        return "hi " + name + " ,i am from port:" + port;
//    }

//    @RequestMapping("/demo/hi")
//    public String qsw(@RequestParam(value = "name", defaultValue = "forezp") String name) {
//        return "hi " + name + " ,i am from port:" + port;
//    }
}



